-- phpMyAdmin SQL Dump
-- version 4.6.5.2
-- https://www.phpmyadmin.net/
--
-- Хост: 127.0.0.1:3306
-- Время создания: Ноя 19 2017 г., 16:08
-- Версия сервера: 5.5.53
-- Версия PHP: 5.6.29

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `slim`
--

-- --------------------------------------------------------

--
-- Структура таблицы `games`
--

CREATE TABLE `games` (
  `id` int(11) NOT NULL,
  `date` varchar(300) NOT NULL,
  `finish` tinyint(1) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `games`
--

INSERT INTO `games` (`id`, `date`, `finish`) VALUES
(1, '2017-11-19 04:00:00.000000', 0),
(2, '2017-11-18 19:00:00.000000', 0),
(3, '2017-11-18 10:30:00.000000', 0),
(4, '2017-11-18 04:00:00.000000', 0),
(5, '2017-11-17 19:00:00.000000', 0),
(6, '2017-11-17 19:00:00.000000', 0),
(7, '2017-11-17 18:45:00.000000', 0),
(8, '2017-11-16 22:00:00.000000', 0),
(9, '2017-11-16 22:00:00.000000', 0),
(10, '2017-11-16 04:00:00.000000', 1),
(11, '2017-11-16 03:30:00.000000', 1),
(12, '2017-11-16 02:00:00.000000', 1),
(13, '2017-11-15 20:30:00.000000', 1),
(14, '2017-11-15 20:00:00.000000', 0),
(15, '2017-11-15 20:00:00.000000', 1),
(16, '2017-11-15 20:00:00.000000', 1),
(17, '2017-11-15 19:35:00.000000', 1),
(18, '2017-11-15 19:10:00.000000', 1),
(19, '2017-11-15 19:00:00.000000', 1),
(20, '2017-11-15 18:45:00.000000', 1);

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `games`
--
ALTER TABLE `games`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `games`
--
ALTER TABLE `games`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
